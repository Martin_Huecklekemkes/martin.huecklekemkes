# About me

My name is Martin and currently I am student at the University of Applied Science Rhein-Waal. I study Medieninformatik in 5th semester.

I live near Kamp-Lintfort.

___

## Interests

I am interested in web development and dev ops on the software side.

Further, I am interested in tinkering and DIY making. I have some practical experience with soldering and Arduino as well as some theoretical knowledge about 3D printing and CNC milling. However, until now, I didn't have the chance to use of these technologies in practice.

I had planned to build my own CNC mill, but due to lack of time and some uncertainties, I have not yet realized this project.
