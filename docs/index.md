# Home

## Hello, stranger!

![](images/FabLab_Text_croped_tranparent.png)

## Welcome to my Fab Academy site

This is my student blog for the Fab Academy.

In this blog I will document all the work I do as part of the course.

&nbsp;

## [Who am I?](about/index.md)

&nbsp;

## [See work](assignments/week01.md)

