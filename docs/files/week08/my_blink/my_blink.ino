#include <SoftwareSerial.h>

#define LED_PIN 13
#define BUTTON_PIN 8
#define rxPin
#define txPin

int counter = 0;
const int maxCount = 5;

int buttonPushCounter = 0;
int buttonState = 0;
int lastButtonState = 0;

void setup() {
  pinMode(LED_PIN, OUTPUT);
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  Serial.begin(9600);
}

void loop() {

  if (counter < maxCount) {
    blink();
    counter++;
    Serial.print("Blink #: ");
    Serial.println(counter);
  }
  checkButton();
}

void checkButton() {
  buttonState = digitalRead(BUTTON_PIN);

  if (buttonState != lastButtonState) {
    if (buttonState == HIGH) {
      buttonPushCounter++;
      Serial.println("pushed");
      Serial.print("number of button pushes: ");
      Serial.println(buttonPushCounter);
      counter = 0;
    }
    delay(50);
  }
  lastButtonState = buttonState;
}

void blink() {
  digitalWrite(LED_PIN, HIGH);
  delay(500);
  digitalWrite(LED_PIN, LOW);
  delay(250);
}
