#include <SoftwareSerial.h>

#define LED_PIN 1
#define BUTTON_PIN 9
#define rxPin 2
#define txPin 3

int counter = 0;
const int maxCount = 5;

int buttonPushCounter = 0;
int buttonState = 0;
int lastButtonState = 0;

SoftwareSerial sws =  SoftwareSerial(rxPin, txPin);

void setup() {
  pinMode(LED_PIN, OUTPUT);
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  sws.begin(4800);
}

void loop() {

  if (counter < maxCount) {
    blink(500, 250);
    counter++;
    sws.print("Blink #: ");
    sws.println(counter);
  }
  checkButton();
}

void checkButton() {
  buttonState = digitalRead(BUTTON_PIN);

  if (buttonState != lastButtonState) {
    if (buttonState == HIGH) {
      buttonPushCounter++;
      sws.println("pushed");
      sws.print("number of button pushes: ");
      sws.println(buttonPushCounter);
      counter = 0;
      for (int i = 0; i < 5; i++) {
        blink(200, 100);
      }

    }
    delay(50);
  }
  lastButtonState = buttonState;
}

void blink(int on, int off) {
  digitalWrite(LED_PIN, HIGH);
  delay(on);
  digitalWrite(LED_PIN, LOW);
  delay(off);
}
