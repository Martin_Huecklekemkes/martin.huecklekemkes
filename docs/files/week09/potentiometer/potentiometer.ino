#include <SoftwareSerial.h>

#define LED_PIN 1
#define BUTTON_PIN 9
#define rxPin 2
#define txPin 3
#define poti 7

int potVal = 0;

SoftwareSerial sws =  SoftwareSerial(rxPin, txPin);

void setup() {
  pinMode(LED_PIN, OUTPUT);
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  pinMode(poti, INPUT);
  sws.begin(4800);
  initBlink();
}

void loop() {
  potVal = readPoti();
  sws.println("Resistance:");
  sws.println(potVal);
  blink(potVal);
}

void initBlink() {
  for (int i = 0; i < 5; i++) {
    digitalWrite(LED_PIN, HIGH);
    delay(200);
    digitalWrite(LED_PIN, LOW);
    delay(50);
  }
}

void blink(int delayValue) {
  sws.print("before mapping");
  sws.println(delayValue);
  delayValue = map(delayValue, 0, 1023, 100, 2000);
  sws.print("after mapping");
  sws.println(delayValue);
  digitalWrite(LED_PIN, HIGH);
  delay(delayValue);
  digitalWrite(LED_PIN, LOW);
  delay(delayValue);
  
}

int readPoti() {
  return analogRead(poti);
}
