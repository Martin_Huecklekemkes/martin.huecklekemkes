#include <Stepper.h>


#define LED_PIN 1
#define BUTTON_PIN 9
#define ENABLE_PIN 2 //yellow
#define STEP_PIN 7 // blue
#define DIR_PIN 8 // green

bool dir = false;

int buttonState = 0;         // current state of the button
int lastButtonState = 0;     // previous state of the button

#define STEPS 100

Stepper stepper(STEPS, 8, 9, 10, 11);

void setup() {
  pinMode(1, OUTPUT); // LED_PIN
  pinMode(2, OUTPUT); // ENABLE_PIN
  pinMode(7, OUTPUT); //STEP_PIN
  pinMode(8, OUTPUT); //DIR_PIN
  pinMode(BUTTON_PIN, INPUT_PULLUP);

  digitalWrite(2, LOW);

}

void loop() {

  buttonState = digitalRead(BUTTON_PIN);

  // compare the buttonState to its previous state
  if (buttonState != lastButtonState) {
    // if the state has changed, increment the counter
    if (buttonState == LOW) {
      // if the current state is LOW then the button went from off to on:
      
      dir = !dir;
    } else {
      // if the current state is HIGH then the button went from on to off:
      
    }
    // Delay a little bit to avoid bouncing
 
  }
  // save the current state as the last state, for next time through the loop
  lastButtonState = buttonState;

  if (dir == true) {
    digitalWrite(8, HIGH); // im Uhrzeigersinn
  } else {
    digitalWrite(8, LOW); // im Uhrzeigersinn
  }

  digitalWrite(7, HIGH);
  digitalWrite(1, HIGH);
  delayMicroseconds(500);
  digitalWrite(7, LOW);
  digitalWrite(1, LOW);
  delayMicroseconds(500);
}
