EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 16502 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2600 6600 700  6600
Wire Wire Line
	2600 6700 700  6700
Wire Wire Line
	2600 6900 700  6900
Wire Wire Line
	2600 7000 700  7000
Wire Wire Line
	1200 5500 700  5500
Wire Wire Line
	1850 4150 2250 4150
Wire Wire Line
	2750 5050 3050 5050
Wire Wire Line
	3050 5050 3550 5050
Wire Wire Line
	4050 5050 3550 5050
Wire Wire Line
	3550 4750 3550 5050
Wire Wire Line
	3050 4750 3050 5050
Wire Wire Line
	4050 4750 4050 5050
Connection ~ 3550 5050
Connection ~ 3050 5050
Wire Wire Line
	1250 200  1100 200 
Wire Wire Line
	1100 200  1100 700 
Wire Wire Line
	1250 1200 1100 1200
Wire Wire Line
	1100 1200 1100 700 
Wire Wire Line
	8300 2450 8650 2450
Wire Wire Line
	8800 4000 8900 4000
Wire Wire Line
	8900 4000 8900 4200
Wire Wire Line
	8900 4200 8900 4400
Wire Wire Line
	8900 4200 9100 4200
Connection ~ 8900 4200
Wire Wire Line
	2600 6100 2000 6100
Wire Wire Line
	2000 6100 1500 6100
Wire Wire Line
	1500 6100 1500 6000
Connection ~ 2000 6100
Wire Wire Line
	2600 6300 2000 6300
Wire Wire Line
	2000 6300 1500 6300
Connection ~ 2000 6300
Wire Wire Line
	2600 7700 700  7700
Wire Wire Line
	2600 7800 700  7800
Wire Wire Line
	1200 6000 700  6000
Wire Wire Line
	1200 6300 700  6300
Wire Wire Line
	2600 7500 700  7500
Wire Wire Line
	950  4150 550  4150
Wire Wire Line
	3850 200  3450 200 
Wire Wire Line
	3450 200  3450 700 
Wire Wire Line
	3450 700  3850 700 
Wire Wire Line
	3850 1200 3450 1200
Wire Wire Line
	3450 1200 3450 700 
Wire Wire Line
	3450 700  3150 700 
Connection ~ 3450 700 
Wire Wire Line
	7500 2450 7250 2450
Wire Wire Line
	6500 4300 6300 4300
Wire Wire Line
	6500 3700 6300 3700
Wire Wire Line
	8000 3600 7700 3600
Wire Wire Line
	2750 4150 3050 4150
Wire Wire Line
	3050 4150 3550 4150
Wire Wire Line
	3550 4150 4050 4150
Wire Wire Line
	3050 4450 3050 4150
Wire Wire Line
	3550 4450 3550 4150
Wire Wire Line
	4050 4450 4050 4150
Connection ~ 3050 4150
Connection ~ 3550 4150
Wire Wire Line
	5000 7300 5400 7300
Wire Wire Line
	1550 4150 1350 4150
Wire Wire Line
	1300 5000 700  5000
Wire Wire Line
	8650 2750 8300 2750
Wire Wire Line
	2600 5500 1900 5500
Wire Wire Line
	1900 5500 1600 5500
Wire Wire Line
	1900 5500 1900 5000
Wire Wire Line
	1900 5000 1600 5000
Wire Wire Line
	1900 5000 1900 4600
Wire Wire Line
	1900 4600 800  4600
Connection ~ 1900 5500
Connection ~ 1900 5000
Wire Wire Line
	5000 5500 5400 5500
Wire Wire Line
	5000 5600 5400 5600
Wire Wire Line
	5000 5800 5400 5800
Wire Wire Line
	5000 6000 5400 6000
Wire Wire Line
	5000 6400 5400 6400
Wire Wire Line
	8400 5150 8700 5150
Wire Wire Line
	5000 6600 5400 6600
Wire Wire Line
	5000 6700 5400 6700
Wire Wire Line
	5000 6800 5400 6800
Wire Wire Line
	5000 6900 5400 6900
Wire Wire Line
	5000 7000 5400 7000
Wire Wire Line
	5000 7100 5400 7100
Wire Wire Line
	5000 7600 5400 7600
Wire Wire Line
	5000 7800 5400 7800
Wire Wire Line
	5000 7400 5400 7400
Wire Wire Line
	4150 200  4550 200 
Wire Wire Line
	1650 200  2050 200 
Wire Wire Line
	5000 7500 5400 7500
Wire Wire Line
	4550 700  4150 700 
Wire Wire Line
	1650 700  2050 700 
Wire Wire Line
	5000 7700 5400 7700
Wire Wire Line
	4550 1200 4150 1200
Wire Wire Line
	1650 1200 2050 1200
Wire Wire Line
	5000 5700 5400 5700
Wire Wire Line
	6650 2850 7500 2850
Wire Wire Line
	6650 2950 7500 2950
Wire Wire Line
	6800 4300 7000 4300
Wire Wire Line
	7000 4300 7000 4100
Wire Wire Line
	7000 4300 7300 4300
Wire Wire Line
	7300 4300 7300 3150
Wire Wire Line
	7300 3150 7500 3150
Connection ~ 7000 4300
Wire Wire Line
	6800 3700 7000 3700
Wire Wire Line
	7000 3700 7000 3900
Wire Wire Line
	7000 3700 7150 3700
Wire Wire Line
	7150 3700 7150 3050
Wire Wire Line
	7150 3050 7500 3050
Connection ~ 7000 3700
Wire Wire Line
	7500 2750 7250 2750
Wire Wire Line
	8300 3600 8600 3600
Wire Wire Line
	7500 2550 7250 2550
Wire Wire Line
	7900 4400 7700 4400
Wire Wire Line
	7700 5150 8000 5150
Wire Wire Line
	7500 2650 7250 2650
Wire Wire Line
	7900 4000 7700 4000
Wire Wire Line
	7700 4850 8000 4850
Wire Wire Line
	8400 4000 8200 4000
Wire Wire Line
	8400 4400 8200 4400
Wire Wire Line
	8400 4850 8700 4850
Wire Wire Line
	5000 6500 5400 6500
$Comp
L satshakit-grbl-eagle-import:ATMEGA48_88_168-AU 328-1
U 1 1 ED40FEC7
P 3800 6600
AR Path="/ED40FEC7" Ref="328-1"  Part="1" 
AR Path="/61B30F36/ED40FEC7" Ref="328-1"  Part="1" 
F 0 "328-1" H 2800 7900 59  0000 L TNN
F 1 "ATMEGA328P-AU" H 2800 5200 59  0000 L BNN
F 2 "satshakit-grbl:TQFP32-08" H 3800 6600 50  0001 C CNN
F 3 "" H 3800 6600 50  0001 C CNN
	1    3800 6600
	1    0    0    -1  
$EndComp
$Comp
L satshakit-grbl-eagle-import:CSM-7X-DU CRYSTAL1
U 1 1 DE0EE1F5
P 2000 6200
AR Path="/DE0EE1F5" Ref="CRYSTAL1"  Part="1" 
AR Path="/61B30F36/DE0EE1F5" Ref="CRYSTAL1"  Part="1" 
F 0 "CRYSTAL1" H 2100 6240 59  0000 L BNN
F 1 "16Mhz" H 2100 6100 59  0000 L BNN
F 2 "satshakit-grbl:CSM-7X-DU" H 2000 6200 50  0001 C CNN
F 3 "" H 2000 6200 50  0001 C CNN
	1    2000 6200
	0    -1   -1   0   
$EndComp
$Comp
L satshakit-grbl-eagle-import:0612ZC225MAT2A C1
U 1 1 5C79BD49
P 1200 6000
AR Path="/5C79BD49" Ref="C1"  Part="1" 
AR Path="/61B30F36/5C79BD49" Ref="C1"  Part="1" 
F 0 "C1" H 1164 6109 69  0000 L BNN
F 1 "18pF" H 1125 5692 69  0000 L BNN
F 2 "satshakit-grbl:CAPC3216X178N" H 1200 6000 50  0001 C CNN
F 3 "" H 1200 6000 50  0001 C CNN
	1    1200 6000
	1    0    0    -1  
$EndComp
$Comp
L satshakit-grbl-eagle-import:0612ZC225MAT2A C2
U 1 1 38FD0FF9
P 1200 6300
AR Path="/38FD0FF9" Ref="C2"  Part="1" 
AR Path="/61B30F36/38FD0FF9" Ref="C2"  Part="1" 
F 0 "C2" H 1164 6409 69  0000 L BNN
F 1 "18pF" H 1125 5992 69  0000 L BNN
F 2 "satshakit-grbl:CAPC3216X178N" H 1200 6300 50  0001 C CNN
F 3 "" H 1200 6300 50  0001 C CNN
	1    1200 6300
	1    0    0    -1  
$EndComp
$Comp
L satshakit-grbl-eagle-import:RESISTOR1206 R1
U 1 1 5FC72806
P 1400 5500
AR Path="/5FC72806" Ref="R1"  Part="1" 
AR Path="/61B30F36/5FC72806" Ref="R1"  Part="1" 
F 0 "R1" H 1250 5559 59  0000 L BNN
F 1 "10K" H 1250 5370 59  0000 L BNN
F 2 "satshakit-grbl:1206" H 1400 5500 50  0001 C CNN
F 3 "" H 1400 5500 50  0001 C CNN
	1    1400 5500
	1    0    0    -1  
$EndComp
$Comp
L satshakit-grbl-eagle-import:LED1206 LED_GREEN
U 1 1 741F3F92
P 1650 4150
AR Path="/741F3F92" Ref="LED_GREEN"  Part="1" 
AR Path="/61B30F36/741F3F92" Ref="LED_GREEN1"  Part="1" 
F 0 "LED_GREEN1" V 1790 4070 59  0000 L BNN
F 1 "LED1206" V 1875 4070 59  0000 L BNN
F 2 "satshakit-grbl:1206" H 1650 4150 50  0001 C CNN
F 3 "" H 1650 4150 50  0001 C CNN
	1    1650 4150
	0    1    1    0   
$EndComp
$Comp
L satshakit-grbl-eagle-import:RESISTOR1206 R2
U 1 1 6A2B927F
P 1150 4150
AR Path="/6A2B927F" Ref="R2"  Part="1" 
AR Path="/61B30F36/6A2B927F" Ref="R2"  Part="1" 
F 0 "R2" H 1000 4209 59  0000 L BNN
F 1 "499" H 1000 4020 59  0000 L BNN
F 2 "satshakit-grbl:1206" H 1150 4150 50  0001 C CNN
F 3 "" H 1150 4150 50  0001 C CNN
	1    1150 4150
	-1   0    0    1   
$EndComp
$Comp
L satshakit-grbl-eagle-import:CAP1206 C3
U 1 1 C3EA450F
P 3550 4550
AR Path="/C3EA450F" Ref="C3"  Part="1" 
AR Path="/61B30F36/C3EA450F" Ref="C4"  Part="1" 
F 0 "C4" H 3610 4665 59  0000 L BNN
F 1 "10uF" H 3610 4465 59  0000 L BNN
F 2 "satshakit-grbl:1206" H 3550 4550 50  0001 C CNN
F 3 "" H 3550 4550 50  0001 C CNN
	1    3550 4550
	-1   0    0    1   
$EndComp
$Comp
L satshakit-grbl-eagle-import:CAP1206 C4
U 1 1 674C1B16
P 4050 4550
AR Path="/674C1B16" Ref="C4"  Part="1" 
AR Path="/61B30F36/674C1B16" Ref="C3"  Part="1" 
F 0 "C3" H 4110 4665 59  0000 L BNN
F 1 "1uF" H 4110 4465 59  0000 L BNN
F 2 "satshakit-grbl:1206" H 4050 4550 50  0001 C CNN
F 3 "" H 4050 4550 50  0001 C CNN
	1    4050 4550
	-1   0    0    1   
$EndComp
$Comp
L satshakit-grbl-eagle-import:CAP1206 C5
U 1 1 8E04CE4C
P 3050 4550
AR Path="/8E04CE4C" Ref="C5"  Part="1" 
AR Path="/61B30F36/8E04CE4C" Ref="C5"  Part="1" 
F 0 "C5" H 3110 4665 59  0000 L BNN
F 1 "100nF" H 3110 4465 59  0000 L BNN
F 2 "satshakit-grbl:1206" H 3050 4550 50  0001 C CNN
F 3 "" H 3050 4550 50  0001 C CNN
	1    3050 4550
	-1   0    0    1   
$EndComp
$Comp
L satshakit-grbl-eagle-import:CAP1206 C6
U 1 1 21A596BE
P 1400 5000
AR Path="/21A596BE" Ref="C6"  Part="1" 
AR Path="/61B30F36/21A596BE" Ref="C6"  Part="1" 
F 0 "C6" H 1460 5115 59  0000 L BNN
F 1 "100nF" H 1460 4915 59  0000 L BNN
F 2 "satshakit-grbl:1206" H 1400 5000 50  0001 C CNN
F 3 "" H 1400 5000 50  0001 C CNN
	1    1400 5000
	0    1    1    0   
$EndComp
$Comp
L satshakit-grbl-eagle-import:CAP1206 C7
U 1 1 64A99F0A
P 3950 200
AR Path="/64A99F0A" Ref="C7"  Part="1" 
AR Path="/61B30F36/64A99F0A" Ref="C7"  Part="1" 
F 0 "C7" H 4010 315 59  0000 L BNN
F 1 "100nF" H 4010 115 59  0000 L BNN
F 2 "satshakit-grbl:1206" H 3950 200 50  0001 C CNN
F 3 "" H 3950 200 50  0001 C CNN
	1    3950 200 
	0    1    1    0   
$EndComp
$Comp
L satshakit-grbl-eagle-import:CAP1206 C8
U 1 1 0213C79D
P 3950 700
AR Path="/0213C79D" Ref="C8"  Part="1" 
AR Path="/61B30F36/0213C79D" Ref="C8"  Part="1" 
F 0 "C8" H 4010 815 59  0000 L BNN
F 1 "100nF" H 4010 615 59  0000 L BNN
F 2 "satshakit-grbl:1206" H 3950 700 50  0001 C CNN
F 3 "" H 3950 700 50  0001 C CNN
	1    3950 700 
	0    1    1    0   
$EndComp
$Comp
L satshakit-grbl-eagle-import:CAP1206 C9
U 1 1 501DA122
P 3950 1200
AR Path="/501DA122" Ref="C9"  Part="1" 
AR Path="/61B30F36/501DA122" Ref="C9"  Part="1" 
F 0 "C9" H 4010 1315 59  0000 L BNN
F 1 "100nF" H 4010 1115 59  0000 L BNN
F 2 "satshakit-grbl:1206" H 3950 1200 50  0001 C CNN
F 3 "" H 3950 1200 50  0001 C CNN
	1    3950 1200
	0    1    1    0   
$EndComp
$Comp
L satshakit-grbl-eagle-import:RESISTOR1206 R3
U 1 1 691E95C4
P 1450 200
AR Path="/691E95C4" Ref="R3"  Part="1" 
AR Path="/61B30F36/691E95C4" Ref="R3"  Part="1" 
F 0 "R3" H 1300 259 59  0000 L BNN
F 1 "4.7K" H 1300 70  59  0000 L BNN
F 2 "satshakit-grbl:1206" H 1450 200 50  0001 C CNN
F 3 "" H 1450 200 50  0001 C CNN
	1    1450 200 
	1    0    0    -1  
$EndComp
$Comp
L satshakit-grbl-eagle-import:RESISTOR1206 R4
U 1 1 738A1E18
P 1450 700
AR Path="/738A1E18" Ref="R4"  Part="1" 
AR Path="/61B30F36/738A1E18" Ref="R4"  Part="1" 
F 0 "R4" H 1300 759 59  0000 L BNN
F 1 "4.7K" H 1300 570 59  0000 L BNN
F 2 "satshakit-grbl:1206" H 1450 700 50  0001 C CNN
F 3 "" H 1450 700 50  0001 C CNN
	1    1450 700 
	1    0    0    -1  
$EndComp
$Comp
L satshakit-grbl-eagle-import:RESISTOR1206 R5
U 1 1 03F1CCD5
P 1450 1200
AR Path="/03F1CCD5" Ref="R5"  Part="1" 
AR Path="/61B30F36/03F1CCD5" Ref="R5"  Part="1" 
F 0 "R5" H 1300 1259 59  0000 L BNN
F 1 "4.7K" H 1300 1070 59  0000 L BNN
F 2 "satshakit-grbl:1206" H 1450 1200 50  0001 C CNN
F 3 "" H 1450 1200 50  0001 C CNN
	1    1450 1200
	1    0    0    -1  
$EndComp
$Comp
L satshakit-grbl-eagle-import:CH340GSMD U2
U 1 1 B3A19141
P 7900 2750
AR Path="/B3A19141" Ref="U2"  Part="1" 
AR Path="/61B30F36/B3A19141" Ref="U2"  Part="1" 
F 0 "U2" H 7600 3175 59  0000 L BNN
F 1 "CH340G" H 7600 2150 59  0000 L BNN
F 2 "satshakit-grbl:SO016" H 7900 2750 50  0001 C CNN
F 3 "" H 7900 2750 50  0001 C CNN
	1    7900 2750
	1    0    0    -1  
$EndComp
$Comp
L satshakit-grbl-eagle-import:CSM-7X-DU CRYSTAL
U 1 1 277F45D2
P 7000 4000
AR Path="/277F45D2" Ref="CRYSTAL"  Part="1" 
AR Path="/61B30F36/277F45D2" Ref="CRYSTAL2"  Part="1" 
F 0 "CRYSTAL2" H 7100 4040 59  0000 L BNN
F 1 "12MHZ" H 7100 3900 59  0000 L BNN
F 2 "satshakit-grbl:CSM-7X-DU" H 7000 4000 50  0001 C CNN
F 3 "" H 7000 4000 50  0001 C CNN
	1    7000 4000
	0    1    1    0   
$EndComp
$Comp
L satshakit-grbl-eagle-import:CAP1206 C10
U 1 1 9ECBC615
P 6600 4300
AR Path="/9ECBC615" Ref="C10"  Part="1" 
AR Path="/61B30F36/9ECBC615" Ref="C10"  Part="1" 
F 0 "C10" H 6660 4415 59  0000 L BNN
F 1 "18pF" H 6660 4215 59  0000 L BNN
F 2 "satshakit-grbl:1206" H 6600 4300 50  0001 C CNN
F 3 "" H 6600 4300 50  0001 C CNN
	1    6600 4300
	0    1    1    0   
$EndComp
$Comp
L satshakit-grbl-eagle-import:CAP1206 C11
U 1 1 A4A71337
P 6600 3700
AR Path="/A4A71337" Ref="C11"  Part="1" 
AR Path="/61B30F36/A4A71337" Ref="C11"  Part="1" 
F 0 "C11" H 6660 3815 59  0000 L BNN
F 1 "18pF" H 6660 3615 59  0000 L BNN
F 2 "satshakit-grbl:1206" H 6600 3700 50  0001 C CNN
F 3 "" H 6600 3700 50  0001 C CNN
	1    6600 3700
	0    1    1    0   
$EndComp
$Comp
L satshakit-grbl-eagle-import:CAP1206 C13
U 1 1 8CC800AB
P 8100 3600
AR Path="/8CC800AB" Ref="C13"  Part="1" 
AR Path="/61B30F36/8CC800AB" Ref="C13"  Part="1" 
F 0 "C13" H 8160 3715 59  0000 L BNN
F 1 "10nF" H 8160 3515 59  0000 L BNN
F 2 "satshakit-grbl:1206" H 8100 3600 50  0001 C CNN
F 3 "" H 8100 3600 50  0001 C CNN
	1    8100 3600
	0    1    1    0   
$EndComp
$Comp
L satshakit-grbl-eagle-import:RES-US1206 R6
U 1 1 34A178D3
P 8600 4000
AR Path="/34A178D3" Ref="R6"  Part="1" 
AR Path="/61B30F36/34A178D3" Ref="R6"  Part="1" 
F 0 "R6" H 8450 4059 59  0000 L BNN
F 1 "2K7" H 8450 3870 59  0000 L BNN
F 2 "satshakit-grbl:R1206" H 8600 4000 50  0001 C CNN
F 3 "" H 8600 4000 50  0001 C CNN
	1    8600 4000
	1    0    0    -1  
$EndComp
$Comp
L satshakit-grbl-eagle-import:RES-US1206 R7
U 1 1 C93C8ECD
P 8600 4400
AR Path="/C93C8ECD" Ref="R7"  Part="1" 
AR Path="/61B30F36/C93C8ECD" Ref="R7"  Part="1" 
F 0 "R7" H 8450 4459 59  0000 L BNN
F 1 "2K7" H 8450 4270 59  0000 L BNN
F 2 "satshakit-grbl:R1206" H 8600 4400 50  0001 C CNN
F 3 "" H 8600 4400 50  0001 C CNN
	1    8600 4400
	1    0    0    -1  
$EndComp
$Comp
L satshakit-grbl-eagle-import:RES-US1206 R8
U 1 1 44057106
P 8200 5150
AR Path="/44057106" Ref="R8"  Part="1" 
AR Path="/61B30F36/44057106" Ref="R8"  Part="1" 
F 0 "R8" H 8050 5209 59  0000 L BNN
F 1 "1K" H 8050 5020 59  0000 L BNN
F 2 "satshakit-grbl:R1206" H 8200 5150 50  0001 C CNN
F 3 "" H 8200 5150 50  0001 C CNN
	1    8200 5150
	1    0    0    -1  
$EndComp
$Comp
L satshakit-grbl-eagle-import:RES-US1206 R9
U 1 1 566DAAD1
P 8200 4850
AR Path="/566DAAD1" Ref="R9"  Part="1" 
AR Path="/61B30F36/566DAAD1" Ref="R9"  Part="1" 
F 0 "R9" H 8050 4909 59  0000 L BNN
F 1 "1K" H 8050 4720 59  0000 L BNN
F 2 "satshakit-grbl:R1206" H 8200 4850 50  0001 C CNN
F 3 "" H 8200 4850 50  0001 C CNN
	1    8200 4850
	1    0    0    -1  
$EndComp
$Comp
L satshakit-grbl-eagle-import:LED LED_RX
U 1 1 2944E868
P 8000 4000
AR Path="/2944E868" Ref="LED_RX"  Part="1" 
AR Path="/61B30F36/2944E868" Ref="LED_RX1"  Part="1" 
F 0 "LED_RX1" V 8140 3920 59  0000 L BNN
F 1 "YELLOW" V 8225 3920 59  0000 L BNN
F 2 "satshakit-grbl:LED1206" H 8000 4000 50  0001 C CNN
F 3 "" H 8000 4000 50  0001 C CNN
	1    8000 4000
	0    1    1    0   
$EndComp
$Comp
L satshakit-grbl-eagle-import:LED LED_TX
U 1 1 6A1081B3
P 8000 4400
AR Path="/6A1081B3" Ref="LED_TX"  Part="1" 
AR Path="/61B30F36/6A1081B3" Ref="LED_TX1"  Part="1" 
F 0 "LED_TX1" V 8140 4320 59  0000 L BNN
F 1 "YELLOW" V 8225 4320 59  0000 L BNN
F 2 "satshakit-grbl:LED1206" H 8000 4400 50  0001 C CNN
F 3 "" H 8000 4400 50  0001 C CNN
	1    8000 4400
	0    1    1    0   
$EndComp
Text GLabel 5400 7600 2    50   Input ~ 0
LIMIT-Z
Text GLabel 5400 7500 2    50   Input ~ 0
LIMIT-Y
Text GLabel 5400 7400 2    50   Input ~ 0
LIMIT-X
Text GLabel 5400 7300 2    50   Input ~ 0
STEPPER-ENABLE
Text GLabel 5400 7800 2    50   Input ~ 0
SPINDLE-DIRECTION
Text GLabel 5400 7700 2    50   Input ~ 0
SPINDLE-ENABLE
Text GLabel 5400 6900 2    50   Input ~ 0
DIR-X
Text GLabel 5400 7000 2    50   Input ~ 0
DIR-Y
Text GLabel 5400 7100 2    50   Input ~ 0
DIR-Z
Text GLabel 5400 6600 2    50   Input ~ 0
STEP-X
Text GLabel 5400 6700 2    50   Input ~ 0
STEP-Y
Text GLabel 5400 6800 2    50   Input ~ 0
STEP-Z
Text GLabel 5400 6400 2    50   Input ~ 0
TX-340
Text GLabel 5400 6500 2    50   Input ~ 0
RX-340
Text GLabel 5400 6000 2    50   Input ~ 0
PROBE
Text GLabel 5400 5800 2    50   Input ~ 0
COOLANT-EN
Text GLabel 5400 5500 2    50   Input ~ 0
RESET-ABORT
Text GLabel 5400 5600 2    50   Input ~ 0
FEED-HOLD
Text GLabel 5400 5700 2    50   Input ~ 0
START-RESUME
Text GLabel 700  7800 0    50   Input ~ 0
GND
Text GLabel 700  7700 0    50   Input ~ 0
GND
Text GLabel 700  7500 0    50   Input ~ 0
GND
Text GLabel 700  6300 0    50   Input ~ 0
GND
Text GLabel 700  6000 0    50   Input ~ 0
GND
Text GLabel 700  6600 0    50   Input ~ 0
5V
Text GLabel 700  6700 0    50   Input ~ 0
5V
Text GLabel 700  6900 0    50   Input ~ 0
5V
Text GLabel 700  7000 0    50   Input ~ 0
5V
Text GLabel 700  5500 0    50   Input ~ 0
5V
Text GLabel 800  4600 0    50   Input ~ 0
RESET
Text GLabel 700  5000 0    50   Input ~ 0
RST-FTDI
Text GLabel 3150 700  0    50   Input ~ 0
GND
Text GLabel 550  4150 0    50   Input ~ 0
GND
Text GLabel 2050 200  2    50   Input ~ 0
LIMIT-X
Text GLabel 2050 700  2    50   Input ~ 0
LIMIT-Y
Text GLabel 2050 1200 2    50   Input ~ 0
LIMIT-Z
Text GLabel 4550 200  2    50   Input ~ 0
LIMIT-X
Text GLabel 4550 700  2    50   Input ~ 0
LIMIT-Y
Text GLabel 4550 1200 2    50   Input ~ 0
LIMIT-Z
Text GLabel 850  700  0    50   Input ~ 0
5V
Text GLabel 2250 4150 2    50   Input ~ 0
5V
Text GLabel 2750 5050 0    50   Input ~ 0
5V
Text GLabel 2750 4150 0    50   Input ~ 0
GND
Text GLabel 4550 2200 0    50   Input ~ 0
PROBE
Text GLabel 4550 2300 0    50   Input ~ 0
COOLANT-EN
Text GLabel 7700 3600 0    50   Input ~ 0
GND
Text GLabel 8600 3600 2    50   Input ~ 0
V3
Text GLabel 8700 5150 2    50   Input ~ 0
TX-340
Text GLabel 8700 4850 2    50   Input ~ 0
RX-340
Text GLabel 7700 4850 0    50   Input ~ 0
RXD
Text GLabel 7700 5150 0    50   Input ~ 0
TXD
Text GLabel 8650 2450 2    50   Input ~ 0
5V
Text GLabel 7250 2450 0    50   Input ~ 0
GND
Text GLabel 7250 2550 0    50   Input ~ 0
TXD
Text GLabel 7250 2650 0    50   Input ~ 0
RXD
Text GLabel 7250 2750 0    50   Input ~ 0
V3
Text GLabel 8650 2750 2    50   Input ~ 0
RST-FTDI
Text GLabel 6650 2650 2    50   Input ~ 0
5V
Text GLabel 6350 3250 3    50   Input ~ 0
GND
Text GLabel 6300 4000 0    50   Input ~ 0
GND
Text GLabel 7700 4000 0    50   Input ~ 0
RXD
Text GLabel 7700 4400 0    50   Input ~ 0
TXD
Text GLabel 9100 4200 2    50   Input ~ 0
5V
Text GLabel 5500 1450 0    50   Input ~ 0
TX-340
Text GLabel 5500 1550 0    50   Input ~ 0
RX-340
Text GLabel 1800 1900 0    50   Input ~ 0
GND
Wire Wire Line
	1100 3450 1100 3350
Wire Wire Line
	1100 2700 1100 2600
Wire Wire Line
	1100 2000 1100 1900
Text GLabel 1100 3350 0    50   Input ~ 0
LIMIT-X
Text GLabel 1100 2600 0    50   Input ~ 0
LIMIT-Y
Text GLabel 1100 1900 0    50   Input ~ 0
LIMIT-Z
$Comp
L Connector:Conn_01x08_Female X-Driver-Crtl1
U 1 1 62052A46
P 11700 2200
F 0 "X-Driver-Crtl1" V 11900 1750 50  0000 L CNN
F 1 "Conn_01x08_Female" V 11800 1750 50  0000 L CNN
F 2 "eagle:08P" H 11700 2200 50  0001 C CNN
F 3 "~" H 11700 2200 50  0001 C CNN
	1    11700 2200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female X-Driver-Pwr1
U 1 1 620536BA
P 12850 2200
F 0 "X-Driver-Pwr1" V 13050 1750 50  0000 L CNN
F 1 "Conn_01x08_Female" V 12950 1750 50  0000 L CNN
F 2 "eagle:08P" H 12850 2200 50  0001 C CNN
F 3 "~" H 12850 2200 50  0001 C CNN
	1    12850 2200
	1    0    0    -1  
$EndComp
Text GLabel 11500 1900 0    50   Input ~ 0
STEPPER-ENABLE
$Comp
L Connector:Conn_01x04_Male X-Driver-Mot1
U 1 1 62062735
P 12350 2200
F 0 "X-Driver-Mot1" V 12200 1750 50  0000 L CNN
F 1 "Conn_01x04_Male" V 12300 1750 50  0000 L CNN
F 2 "eagle:04P" H 12350 2200 50  0001 C CNN
F 3 "~" H 12350 2200 50  0001 C CNN
	1    12350 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	12650 2400 12550 2400
Wire Wire Line
	12550 2300 12650 2300
Wire Wire Line
	12650 2200 12550 2200
Wire Wire Line
	12550 2100 12650 2100
Text GLabel 12650 2500 0    50   Input ~ 0
5V
Text GLabel 12650 2600 0    50   Input ~ 0
GND
Text GLabel 12650 2000 0    50   Input ~ 0
GND
Text GLabel 12650 1900 0    50   Input ~ 0
12V
$Comp
L Connector:Barrel_Jack J1
U 1 1 620D0A45
P 4750 5050
F 0 "J1" H 4807 5375 50  0000 C CNN
F 1 "Barrel_Jack" H 4807 5284 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_CLIFF_FC681465S_SMT_Horizontal_Adjusted" H 4800 5010 50  0001 C CNN
F 3 "~" H 4800 5010 50  0001 C CNN
	1    4750 5050
	1    0    0    -1  
$EndComp
Text GLabel 5050 4950 2    50   Input ~ 0
GND
Text GLabel 5050 5150 2    50   Input ~ 0
12V
Text GLabel 11500 2000 0    50   Input ~ 0
X-MS1
Text GLabel 11500 2100 0    50   Input ~ 0
X-MS2
Text GLabel 11500 2200 0    50   Input ~ 0
X-MS3
Wire Wire Line
	11500 2300 11500 2400
Text GLabel 11500 2600 0    50   Input ~ 0
DIR-X
Text GLabel 11500 2500 0    50   Input ~ 0
STEP-X
$Comp
L Connector:Conn_01x08_Female Y1-Driver-Crtl1
U 1 1 620FCC0D
P 11700 3500
F 0 "Y1-Driver-Crtl1" V 11900 3050 50  0000 L CNN
F 1 "Conn_01x08_Female" V 11800 3050 50  0000 L CNN
F 2 "eagle:08P" H 11700 3500 50  0001 C CNN
F 3 "~" H 11700 3500 50  0001 C CNN
	1    11700 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female Y1-Driver-Pwr1
U 1 1 620FD34F
P 12850 3500
F 0 "Y1-Driver-Pwr1" V 13050 3050 50  0000 L CNN
F 1 "Conn_01x08_Female" V 12950 3050 50  0000 L CNN
F 2 "eagle:08P" H 12850 3500 50  0001 C CNN
F 3 "~" H 12850 3500 50  0001 C CNN
	1    12850 3500
	1    0    0    -1  
$EndComp
Text GLabel 11500 3200 0    50   Input ~ 0
STEPPER-ENABLE
$Comp
L Connector:Conn_01x04_Male Y1-Driver-Mot1
U 1 1 620FD35A
P 12350 3500
F 0 "Y1-Driver-Mot1" V 12200 3050 50  0000 L CNN
F 1 "Conn_01x04_Male" V 12300 3050 50  0000 L CNN
F 2 "eagle:04P" H 12350 3500 50  0001 C CNN
F 3 "~" H 12350 3500 50  0001 C CNN
	1    12350 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	12650 3700 12550 3700
Wire Wire Line
	12550 3600 12650 3600
Wire Wire Line
	12650 3500 12550 3500
Wire Wire Line
	12550 3400 12650 3400
Text GLabel 12650 3800 0    50   Input ~ 0
5V
Text GLabel 12650 3900 0    50   Input ~ 0
GND
Text GLabel 12650 3300 0    50   Input ~ 0
GND
Text GLabel 12650 3200 0    50   Input ~ 0
12V
Text GLabel 11500 3300 0    50   Input ~ 0
Y1-MS1
Text GLabel 11500 3400 0    50   Input ~ 0
Y1-MS2
Text GLabel 11500 3500 0    50   Input ~ 0
Y1-MS3
Wire Wire Line
	11500 3600 11500 3700
Text GLabel 11500 3900 0    50   Input ~ 0
DIR-Y
Text GLabel 11500 3800 0    50   Input ~ 0
STEP-Y
$Comp
L Connector:Conn_01x08_Female Y2-Driver-Crtl1
U 1 1 6210DF96
P 11700 4750
F 0 "Y2-Driver-Crtl1" V 11900 4300 50  0000 L CNN
F 1 "Conn_01x08_Female" V 11800 4300 50  0000 L CNN
F 2 "eagle:08P" H 11700 4750 50  0001 C CNN
F 3 "~" H 11700 4750 50  0001 C CNN
	1    11700 4750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female Y2-Driver-Pwr1
U 1 1 6210E76E
P 12850 4750
F 0 "Y2-Driver-Pwr1" V 13050 4300 50  0000 L CNN
F 1 "Conn_01x08_Female" V 12950 4300 50  0000 L CNN
F 2 "eagle:08P" H 12850 4750 50  0001 C CNN
F 3 "~" H 12850 4750 50  0001 C CNN
	1    12850 4750
	1    0    0    -1  
$EndComp
Text GLabel 11500 4450 0    50   Input ~ 0
STEPPER-ENABLE
$Comp
L Connector:Conn_01x04_Male Y2-Driver-Mot1
U 1 1 6210E779
P 12350 4750
F 0 "Y2-Driver-Mot1" V 12200 4300 50  0000 L CNN
F 1 "Conn_01x04_Male" V 12300 4300 50  0000 L CNN
F 2 "eagle:04P" H 12350 4750 50  0001 C CNN
F 3 "~" H 12350 4750 50  0001 C CNN
	1    12350 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	12650 4950 12550 4950
Wire Wire Line
	12550 4850 12650 4850
Wire Wire Line
	12650 4750 12550 4750
Wire Wire Line
	12550 4650 12650 4650
Text GLabel 12650 5050 0    50   Input ~ 0
5V
Text GLabel 12650 5150 0    50   Input ~ 0
GND
Text GLabel 12650 4550 0    50   Input ~ 0
GND
Text GLabel 12650 4450 0    50   Input ~ 0
12V
Wire Wire Line
	11500 4850 11500 4950
Text GLabel 11500 5150 0    50   Input ~ 0
DIR-Y
Text GLabel 11500 5050 0    50   Input ~ 0
STEP-Y
$Comp
L Connector:Conn_01x08_Female Z-Driver-Crtl1
U 1 1 6210E791
P 11700 6000
F 0 "Z-Driver-Crtl1" V 11900 5550 50  0000 L CNN
F 1 "Conn_01x08_Female" V 11800 5550 50  0000 L CNN
F 2 "eagle:08P" H 11700 6000 50  0001 C CNN
F 3 "~" H 11700 6000 50  0001 C CNN
	1    11700 6000
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x08_Female Z-Driver-Pwr1
U 1 1 6210E79B
P 12850 6000
F 0 "Z-Driver-Pwr1" V 13050 5550 50  0000 L CNN
F 1 "Conn_01x08_Female" V 12950 5550 50  0000 L CNN
F 2 "eagle:08P" H 12850 6000 50  0001 C CNN
F 3 "~" H 12850 6000 50  0001 C CNN
	1    12850 6000
	1    0    0    -1  
$EndComp
Text GLabel 11500 5700 0    50   Input ~ 0
STEPPER-ENABLE
$Comp
L Connector:Conn_01x04_Male Z-Driver-Mot1
U 1 1 6210E7A6
P 12350 6000
F 0 "Z-Driver-Mot1" V 12200 5550 50  0000 L CNN
F 1 "Conn_01x04_Male" V 12300 5550 50  0000 L CNN
F 2 "eagle:04P" H 12350 6000 50  0001 C CNN
F 3 "~" H 12350 6000 50  0001 C CNN
	1    12350 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	12650 6200 12550 6200
Wire Wire Line
	12550 6100 12650 6100
Wire Wire Line
	12650 6000 12550 6000
Wire Wire Line
	12550 5900 12650 5900
Text GLabel 12650 6300 0    50   Input ~ 0
5V
Text GLabel 12650 6400 0    50   Input ~ 0
GND
Text GLabel 12650 5800 0    50   Input ~ 0
GND
Text GLabel 12650 5700 0    50   Input ~ 0
12V
Text GLabel 11500 5800 0    50   Input ~ 0
Z-MS1
Text GLabel 11500 5900 0    50   Input ~ 0
Z-MS2
Text GLabel 11500 6000 0    50   Input ~ 0
Z-MS3
Wire Wire Line
	11500 6100 11500 6200
Text GLabel 11500 6400 0    50   Input ~ 0
DIR-Z
Text GLabel 11500 6300 0    50   Input ~ 0
STEP-Z
Text GLabel 11500 4750 0    50   Input ~ 0
Y2-MS3
Text GLabel 11500 4650 0    50   Input ~ 0
Y2-MS2
Text GLabel 11500 4550 0    50   Input ~ 0
Y2-MS1
Text GLabel 10300 6400 2    50   Input ~ 0
GND
Text GLabel 10300 5600 2    50   Input ~ 0
Z-MS1
Text GLabel 10300 5950 2    50   Input ~ 0
Z-MS2
Text GLabel 10300 6300 2    50   Input ~ 0
Z-MS3
Text GLabel 10300 5050 2    50   Input ~ 0
Y2-MS3
Text GLabel 10300 4700 2    50   Input ~ 0
Y2-MS2
Text GLabel 10300 4350 2    50   Input ~ 0
Y2-MS1
Text GLabel 10300 3100 2    50   Input ~ 0
Y1-MS1
Text GLabel 10300 3450 2    50   Input ~ 0
Y1-MS2
Text GLabel 10300 3800 2    50   Input ~ 0
Y1-MS3
Text GLabel 10300 1850 2    50   Input ~ 0
X-MS1
Text GLabel 10300 2200 2    50   Input ~ 0
X-MS2
Text GLabel 10300 2550 2    50   Input ~ 0
X-MS3
$Comp
L Connector:Conn_01x03_Male Work-Crtl1
U 1 1 6238705E
P 5950 650
F 0 "Work-Crtl1" V 5800 500 50  0000 L CNN
F 1 "Conn_01x03_Male" V 5900 500 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical_0.91_hole" H 5950 650 50  0001 C CNN
F 3 "~" H 5950 650 50  0001 C CNN
	1    5950 650 
	-1   0    0    -1  
$EndComp
Text GLabel 4550 2900 0    50   Input ~ 0
SPINDLE-DIRECTION
Text GLabel 4550 3000 0    50   Input ~ 0
SPINDLE-ENABLE
Text GLabel 5750 750  0    50   Input ~ 0
RESET-ABORT
Text GLabel 5750 650  0    50   Input ~ 0
FEED-HOLD
Text GLabel 5750 550  0    50   Input ~ 0
START-RESUME
$Comp
L Connector:Conn_01x02_Male Serial1
U 1 1 625C1373
P 5700 1450
F 0 "Serial1" V 5550 1300 50  0000 L CNN
F 1 "Conn_01x02_Male" V 5650 1300 50  0000 L CNN
F 2 "eagle:02PA" H 5700 1450 50  0001 C CNN
F 3 "~" H 5700 1450 50  0001 C CNN
	1    5700 1450
	-1   0    0    -1  
$EndComp
Text GLabel 5250 2300 0    50   Input ~ 0
GND
$Comp
L Connector:Conn_01x02_Male Spindel-Crtl1
U 1 1 6272F8C0
P 4750 2900
F 0 "Spindel-Crtl1" V 4600 2400 50  0000 L CNN
F 1 "Conn_01x02_Male" V 4700 2400 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 4750 2900 50  0001 C CNN
F 3 "~" H 4750 2900 50  0001 C CNN
	1    4750 2900
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male Utility-Crtl1
U 1 1 62746646
P 4750 2200
F 0 "Utility-Crtl1" V 4600 1700 50  0000 L CNN
F 1 "Conn_01x02_Male" V 4700 1700 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 4750 2200 50  0001 C CNN
F 3 "~" H 4750 2200 50  0001 C CNN
	1    4750 2200
	-1   0    0    -1  
$EndComp
$Comp
L Device:CP C12-X-CAP1
U 1 1 62753C7C
P 12800 1700
F 0 "C12-X-CAP1" V 12545 1700 50  0000 C CNN
F 1 "CP100µF" V 12636 1700 50  0000 C CNN
F 2 "eagle:02P" H 12838 1550 50  0001 C CNN
F 3 "~" H 12800 1700 50  0001 C CNN
	1    12800 1700
	0    1    1    0   
$EndComp
Text GLabel 12950 1700 2    50   Input ~ 0
12V
Text GLabel 12650 1700 0    50   Input ~ 0
GND
$Comp
L Device:CP C14-Y1-CAP1
U 1 1 6275C6D1
P 12800 3000
F 0 "C14-Y1-CAP1" V 12545 3000 50  0000 C CNN
F 1 "CP100µF" V 12636 3000 50  0000 C CNN
F 2 "eagle:02P" H 12838 2850 50  0001 C CNN
F 3 "~" H 12800 3000 50  0001 C CNN
	1    12800 3000
	0    1    1    0   
$EndComp
Text GLabel 12950 3000 2    50   Input ~ 0
12V
Text GLabel 12650 3000 0    50   Input ~ 0
GND
$Comp
L Device:CP C15-Y2-CAP1
U 1 1 627CAF02
P 12800 4250
F 0 "C15-Y2-CAP1" V 12545 4250 50  0000 C CNN
F 1 "CP100µF" V 12636 4250 50  0000 C CNN
F 2 "eagle:02P" H 12838 4100 50  0001 C CNN
F 3 "~" H 12800 4250 50  0001 C CNN
	1    12800 4250
	0    1    1    0   
$EndComp
Text GLabel 12950 4250 2    50   Input ~ 0
12V
Text GLabel 12650 4250 0    50   Input ~ 0
GND
$Comp
L Device:CP C16-Z-CAP1
U 1 1 627F1CC1
P 12800 5500
F 0 "C16-Z-CAP1" V 12545 5500 50  0000 C CNN
F 1 "CP100µF" V 12636 5500 50  0000 C CNN
F 2 "eagle:02P" H 12838 5350 50  0001 C CNN
F 3 "~" H 12800 5500 50  0001 C CNN
	1    12800 5500
	0    1    1    0   
$EndComp
Text GLabel 12950 5500 2    50   Input ~ 0
12V
Text GLabel 12650 5500 0    50   Input ~ 0
GND
$Comp
L Connector:Conn_01x02_Male Utility-GND1
U 1 1 62823471
P 5450 2200
F 0 "Utility-GND1" V 5300 1700 50  0000 L CNN
F 1 "Conn_01x02_Male" V 5400 1700 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 5450 2200 50  0001 C CNN
F 3 "~" H 5450 2200 50  0001 C CNN
	1    5450 2200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5250 2300 5250 2200
$Comp
L Connector:Conn_01x02_Male Spindel-GND1
U 1 1 628462A0
P 5450 2900
F 0 "Spindel-GND1" V 5300 2400 50  0000 L CNN
F 1 "Conn_01x02_Male" V 5400 2400 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 5450 2900 50  0001 C CNN
F 3 "~" H 5450 2900 50  0001 C CNN
	1    5450 2900
	-1   0    0    -1  
$EndComp
Text GLabel 5250 3000 0    50   Input ~ 0
GND
Wire Wire Line
	5250 3000 5250 2900
$Comp
L Connector:Conn_01x02_Male J2
U 1 1 61B5A0E5
P 10100 1850
F 0 "J2" H 10208 2031 50  0000 C CNN
F 1 "X-MS1" H 10208 1940 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 10100 1850 50  0001 C CNN
F 3 "~" H 10100 1850 50  0001 C CNN
	1    10100 1850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J3
U 1 1 61B73C33
P 10100 2200
F 0 "J3" H 10208 2381 50  0000 C CNN
F 1 "X-MS2" H 10208 2290 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 10100 2200 50  0001 C CNN
F 3 "~" H 10100 2200 50  0001 C CNN
	1    10100 2200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J4
U 1 1 61B7519D
P 10100 2550
F 0 "J4" H 10208 2731 50  0000 C CNN
F 1 "X-MS3" H 10208 2640 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 10100 2550 50  0001 C CNN
F 3 "~" H 10100 2550 50  0001 C CNN
	1    10100 2550
	1    0    0    -1  
$EndComp
Text GLabel 10300 1950 2    50   Input ~ 0
GND
Text GLabel 10300 2300 2    50   Input ~ 0
GND
Text GLabel 10300 2650 2    50   Input ~ 0
GND
$Comp
L Connector:Conn_01x02_Male J5
U 1 1 61B9E7FF
P 10100 3100
F 0 "J5" H 10208 3281 50  0000 C CNN
F 1 "Y1-MS1" H 10208 3190 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 10100 3100 50  0001 C CNN
F 3 "~" H 10100 3100 50  0001 C CNN
	1    10100 3100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J6
U 1 1 61B9E809
P 10100 3450
F 0 "J6" H 10208 3631 50  0000 C CNN
F 1 "Y1-MS2" H 10208 3540 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 10100 3450 50  0001 C CNN
F 3 "~" H 10100 3450 50  0001 C CNN
	1    10100 3450
	1    0    0    -1  
$EndComp
Text GLabel 10300 3200 2    50   Input ~ 0
GND
Text GLabel 10300 3550 2    50   Input ~ 0
GND
Text GLabel 10300 3900 2    50   Input ~ 0
GND
$Comp
L Connector:Conn_01x02_Male J7
U 1 1 61B9E813
P 10100 3800
F 0 "J7" H 10208 3981 50  0000 C CNN
F 1 "Y1-MS3" H 10208 3890 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 10100 3800 50  0001 C CNN
F 3 "~" H 10100 3800 50  0001 C CNN
	1    10100 3800
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J8
U 1 1 61C0CC5E
P 10100 4350
F 0 "J8" H 10208 4531 50  0000 C CNN
F 1 "Y2-MS1" H 10208 4440 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 10100 4350 50  0001 C CNN
F 3 "~" H 10100 4350 50  0001 C CNN
	1    10100 4350
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J9
U 1 1 61C0CC68
P 10100 4700
F 0 "J9" H 10208 4881 50  0000 C CNN
F 1 "Y2-MS2" H 10208 4790 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 10100 4700 50  0001 C CNN
F 3 "~" H 10100 4700 50  0001 C CNN
	1    10100 4700
	1    0    0    -1  
$EndComp
Text GLabel 10300 4450 2    50   Input ~ 0
GND
Text GLabel 10300 4800 2    50   Input ~ 0
GND
Text GLabel 10300 5150 2    50   Input ~ 0
GND
$Comp
L Connector:Conn_01x02_Male J10
U 1 1 61C0CC75
P 10100 5050
F 0 "J10" H 10208 5231 50  0000 C CNN
F 1 "Y2-MS3" H 10208 5140 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 10100 5050 50  0001 C CNN
F 3 "~" H 10100 5050 50  0001 C CNN
	1    10100 5050
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J11
U 1 1 61C4C41E
P 10100 5600
F 0 "J11" H 10208 5781 50  0000 C CNN
F 1 "Z-MS1" H 10208 5690 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 10100 5600 50  0001 C CNN
F 3 "~" H 10100 5600 50  0001 C CNN
	1    10100 5600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J12
U 1 1 61C4CC9C
P 10100 5950
F 0 "J12" H 10208 6131 50  0000 C CNN
F 1 "Z-MS2" H 10208 6040 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 10100 5950 50  0001 C CNN
F 3 "~" H 10100 5950 50  0001 C CNN
	1    10100 5950
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J13
U 1 1 61C4CCA6
P 10100 6300
F 0 "J13" H 10208 6481 50  0000 C CNN
F 1 "Z-MS3" H 10208 6390 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 10100 6300 50  0001 C CNN
F 3 "~" H 10100 6300 50  0001 C CNN
	1    10100 6300
	1    0    0    -1  
$EndComp
Text GLabel 10300 6050 2    50   Input ~ 0
GND
Text GLabel 10300 5700 2    50   Input ~ 0
GND
$Comp
L Connector:Conn_01x02_Male Limit-Z-Crtl1
U 1 1 61F0D532
P 1300 2000
F 0 "Limit-Z-Crtl1" V 1150 1500 50  0000 L CNN
F 1 "Conn_01x02_Male" V 1250 1500 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 1300 2000 50  0001 C CNN
F 3 "~" H 1300 2000 50  0001 C CNN
	1    1300 2000
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male Limit-Y-Crtl1
U 1 1 61F0DE54
P 1300 2700
F 0 "Limit-Y-Crtl1" V 1150 2200 50  0000 L CNN
F 1 "Conn_01x02_Male" V 1250 2200 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 1300 2700 50  0001 C CNN
F 3 "~" H 1300 2700 50  0001 C CNN
	1    1300 2700
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male Limit-Z-GND1
U 1 1 61F0EC58
P 2000 2000
F 0 "Limit-Z-GND1" V 1850 1500 50  0000 L CNN
F 1 "Conn_01x02_Male" V 1950 1500 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 2000 2000 50  0001 C CNN
F 3 "~" H 2000 2000 50  0001 C CNN
	1    2000 2000
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male Limit-Y-GND1
U 1 1 61F0F292
P 2000 2700
F 0 "Limit-Y-GND1" V 1850 2200 50  0000 L CNN
F 1 "Conn_01x02_Male" V 1950 2200 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 2000 2700 50  0001 C CNN
F 3 "~" H 2000 2700 50  0001 C CNN
	1    2000 2700
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male Limit-X-GND1
U 1 1 61F0F9BE
P 2000 3450
F 0 "Limit-X-GND1" V 1850 2950 50  0000 L CNN
F 1 "Conn_01x02_Male" V 1950 2950 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 2000 3450 50  0001 C CNN
F 3 "~" H 2000 3450 50  0001 C CNN
	1    2000 3450
	-1   0    0    1   
$EndComp
Wire Wire Line
	1800 1900 1800 2000
Wire Wire Line
	1800 2600 1800 2700
Wire Wire Line
	1800 3350 1800 3450
Text GLabel 1800 2600 0    50   Input ~ 0
GND
Text GLabel 1800 3350 0    50   Input ~ 0
GND
$Comp
L Connector:Conn_01x02_Male Limit-X-Crtl1
U 1 1 61F0E64C
P 1300 3450
F 0 "Limit-X-Crtl1" V 1150 2950 50  0000 L CNN
F 1 "Conn_01x02_Male" V 1250 2950 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical_0.91_hole" H 1300 3450 50  0001 C CNN
F 3 "~" H 1300 3450 50  0001 C CNN
	1    1300 3450
	-1   0    0    1   
$EndComp
$Comp
L Connector:USB_B J14
U 1 1 61B95779
P 6350 2850
F 0 "J14" H 6407 3317 50  0000 C CNN
F 1 "USB_B" H 6407 3226 50  0000 C CNN
F 2 "Connector_USB:USB_Mini-B_Wuerth_65100516121_Horizontal" H 6500 2800 50  0001 C CNN
F 3 " ~" H 6500 2800 50  0001 C CNN
	1    6350 2850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male Fan1
U 1 1 61BCB016
P 3200 3250
F 0 "Fan1" V 3050 3100 50  0000 L CNN
F 1 "Conn_01x02_Male" V 3150 3100 50  0000 L CNN
F 2 "eagle:02P" H 3200 3250 50  0001 C CNN
F 3 "~" H 3200 3250 50  0001 C CNN
	1    3200 3250
	0    -1   1    0   
$EndComp
Text GLabel 3300 3450 3    50   Input ~ 0
GND
Text GLabel 3200 3450 3    50   Input ~ 0
12V
$Comp
L Connector:Conn_01x02_Male Fan2
U 1 1 61BCC8B7
P 3200 2600
F 0 "Fan2" V 3050 2450 50  0000 L CNN
F 1 "Conn_01x02_Male" V 3150 2450 50  0000 L CNN
F 2 "eagle:02P" H 3200 2600 50  0001 C CNN
F 3 "~" H 3200 2600 50  0001 C CNN
	1    3200 2600
	0    -1   1    0   
$EndComp
Text GLabel 3300 2800 3    50   Input ~ 0
GND
Text GLabel 3200 2800 3    50   Input ~ 0
12V
$Comp
L Connector:Conn_01x02_Male Fan3
U 1 1 61BD397F
P 3200 1950
F 0 "Fan3" V 3050 1800 50  0000 L CNN
F 1 "Conn_01x02_Male" V 3150 1800 50  0000 L CNN
F 2 "eagle:02P" H 3200 1950 50  0001 C CNN
F 3 "~" H 3200 1950 50  0001 C CNN
	1    3200 1950
	0    -1   1    0   
$EndComp
Text GLabel 3300 2150 3    50   Input ~ 0
GND
Text GLabel 3200 2150 3    50   Input ~ 0
12V
Wire Wire Line
	8900 4400 8800 4400
Connection ~ 1100 700 
Wire Wire Line
	1100 700  1250 700 
Wire Wire Line
	1100 700  850  700 
Wire Notes Line
	6400 5350 6400 8150
Wire Notes Line
	6400 8150 200  8150
Wire Notes Line
	200  8150 200  3950
Wire Notes Line
	5000 50   5000 1650
Wire Notes Line
	5000 1650 2450 1650
Wire Notes Line
	2450 1650 2450 3750
Wire Notes Line
	550  3750 550  50  
Wire Notes Line
	3550 3750 3550 1650
Wire Wire Line
	6650 1500 6450 1500
Wire Wire Line
	6650 1400 6450 1400
$Comp
L satshakit-grbl-eagle-import:M02 RESETS
U 1 1 9A67CDF1
P 6950 1400
AR Path="/9A67CDF1" Ref="RESETS"  Part="1" 
AR Path="/61B30F36/9A67CDF1" Ref="RESETS1"  Part="1" 
F 0 "RESETS1" H 6850 1630 59  0000 L BNN
F 1 "M02" H 6850 1200 59  0000 L BNN
F 2 "eagle:02P" H 6950 1400 50  0001 C CNN
F 3 "" H 6950 1400 50  0001 C CNN
	1    6950 1400
	-1   0    0    1   
$EndComp
Text GLabel 6450 1400 0    50   Input ~ 0
RESET
Text GLabel 6450 1500 0    50   Input ~ 0
RST-FTDI
$Comp
L Connector:Conn_01x03_Male Work-Crtl-GND1
U 1 1 623FEAC1
P 6900 650
F 0 "Work-Crtl-GND1" V 6750 500 50  0000 L CNN
F 1 "Conn_01x03_Male" V 6850 500 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical_0.91_hole" H 6900 650 50  0001 C CNN
F 3 "~" H 6900 650 50  0001 C CNN
	1    6900 650 
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6700 550  6700 650 
Wire Wire Line
	6700 750  6700 650 
Connection ~ 6700 650 
Text GLabel 6700 750  0    50   Input ~ 0
GND
Wire Notes Line
	7450 50   7450 2050
Wire Notes Line
	550  50   7450 50  
Wire Wire Line
	6300 3700 6300 4300
Wire Notes Line
	9250 2050 9250 5350
Wire Notes Line
	13350 1300 9900 1300
Wire Notes Line
	9900 1300 9900 6600
Wire Notes Line
	9900 6600 13350 6600
Wire Notes Line
	13350 6600 13350 1300
Text Notes 11500 1600 2    197  ~ 39
Driver
Text Notes 6700 5000 2    197  ~ 39
USB
Text Notes 4300 2000 2    197  ~ 39
Pins
Text Notes 3400 1550 2    197  ~ 39
Limits
Text Notes 3050 3700 2    197  ~ 39
Fan
Text Notes 4250 4600 0    197  ~ 39
Controller/\nPower
Wire Notes Line
	6000 5350 6000 2050
Wire Notes Line
	6000 5350 9250 5350
Wire Notes Line
	6000 2050 9250 2050
Wire Notes Line
	550  3750 6000 3750
Wire Notes Line
	200  3950 6000 3950
$EndSCHEMATC
