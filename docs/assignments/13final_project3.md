# 13. Final Project: Construction plans

## Task

- construct the mill in Fusion 360
  - keep it simple
  - keep it compact as possible
  - stay within the budget
  - workarea dimensions according to planed requirements

## The design

The plan was to build a cnc mill with a working volume of x: 500mm by y: 400mm by z: 100mm. The x-axis is longer than the y axis to allow milling long objects in subsequent jobs by pushing the workpiece stepwise through the mill. Further a "classic" gantry layout was chosen, where the x-axis is attached to the movable y-axis rather than a movable bed being the y-axis.

Following this principle the z-axis is attached to the movable x-axis carriage and the x-axis is part of the gantry which sits on the y axis, which is connected to the base. This is also the order in which the machine parts where designed to fit the planed workarea dimensions.

![](../images/final_project3/sample.jpg)

Otherwise except some trouble with Fusion360 due to unintuitive behavior, the design phase was without issues.

### Z-Axis

The z-axis holds the spindle and moves up and down. It is attached to the x-carriage.

![](../images/final_project3/zaxis.PNG)

### X-Axis

The x-axis is the main component of the gantry. It moves the x-carriage left to right. The gantry is attached to the rollers of the y-axis.

![](../images/final_project3/xaxis.PNG)

### Y-Axis

The y-axis move back and forth, carries the gantry and is attached to the base. The axis is mirrored and has a stepper motor on either side, to prevent applying uneven forces to the legs of the gantry that could skew it, and thus reduce precision.

![](../images/final_project3/yaxis.PNG)

### Base

The base is an elevated hollow structure to which the guiderods, leadscrews and steppers of the y-axis are attached. Further the sacrificial layer is attached to the top.

![](../images/final_project3/base.PNG)

![](../images/final_project3/mil1.PNG)

## Downloads

The mill plans can be downloaded [here](../files/final_project3/CNC_Mill_v49.f3z).
