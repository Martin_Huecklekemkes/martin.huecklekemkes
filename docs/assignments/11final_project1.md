# 11. Final Project: Outline

## Task

As a finalproject, it was choosen to build a cnc mill.

## Technical data

- Working Area:
  - x: 500mm
  - y: 400mm
  - z: 100mm
- Gantry style mill
- 2 seperate motors for the y axis (for moving the gantry)
- 1 motor for the x axis
- 1 motor for the z axis

## ToDo's

- Create controller board in KiCad
- Fabricate PCB
- Solder PCB
- Test PCB
- Create 3D model with concrete dimensions
- Order Parts
- Build frame
- Assemble everything
