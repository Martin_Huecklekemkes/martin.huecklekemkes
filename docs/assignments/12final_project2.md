# 12. Final Project: The Controller

## Task

- Create a controller board in KiCad that...
  - utilizes a ATMEGA 328p
  - has a USB controller
  - has 4 "slots" for stepper drivers
  - has a 12V dc jack
  - preferably is doublesided to have all i/o on one side

## The PCB

The pcb is partially based on the [cnc controller from Daniele Ingrassia](https://github.com/satshakit/satshakit-grbl).

Especially the usb controller was adopted and some ideas for routing the tracks where derived.

Otherwise, due to the differences in design, for example that the planed PCB has the drivers as interchangeable modules on the topside of the board, there are several tracks that have to cross. Further to have the drivers on the topside placed in female headers, it is necessary to connect the headers from the bottom side. Thus it was necessary to make the PCB doublesided.

## Features

- 12v dc barreljack
- usb-b connector
- Atmega328p for maximum compatibility with grbl
- CH340G USB-controller
- USB programming and serial communication
- endstop noise filtering
- logically grouped pinout
- 2.54mm pitch wire to board connectors compatibility
- 4 "slots" for pololu compatible driver
- duplicated y axis for 2nd motor
- all i/o on the top side

## Schematics

![](../images/final_project2/controller.png)
![](../images/final_project2/drivers.png)
![](../images/final_project2/limits.png)
![](../images/final_project2/USB.png)

## PCB layout

Topside:
![](../images/final_project2/pcb_top.png)

Bottomside:
![](../images/final_project2/pcb_bot.png)

## Milling paths

Topside:
![](../images/final_project2/top.png)
Bottomside:
![](../images/final_project2/bot.png)
Holes:
![](../images/final_project2/holes.png)
Cutout:
![](../images/final_project2/cutout.png)

## Milling

First a MDF piece was milled flat, to have a reliable reference plane for the double sided milling.

![](../images/final_project2/milling/planing.jpg)

Milling the top side yielded good results and was without problems.
Also the engraved letters turned out to be good and readable.
![](../images/final_project2/milling/top.jpg)

To mill the pcb double sided, it was necessary, to use pins to recenter the pcb after flipping. The holes for these pins have been milled deeper, into the underlying MDF, so the pins can be inserted through the pcb into the wood, to re align the pcb when flipped.

![](../images/final_project2/milling/top2.jpg)

![](../images/final_project2/milling/pins.jpg)

![](../images/final_project2/milling/pins2.jpg)

Because the pins had to be removed for milling, to prevent the mill accidentally hitting a pin, double sided tape was again used to fix the pcb to the MDF.

![](../images/final_project2/milling/flip.jpg)

Unfortunately, on the bottom side, there were some problems probably with getting the z-height correct, that resulted in inconsistent width of the traces ranging to completely removed traces which caused some pins to not have a connection. However, soldering jumperwires seemed like a feasible solution.

## Soldering

First, jumperwires to bridge the interrupted traces have been soldered in place, as this was considered the most crucial part for success. If the connections could not have been restored, the PCB would have been to be manufactured again. Further, as the driver "slots" were primarily affected by the broken traces, the femaleheaders also had to be soldered inplace. This was more difficult than expected, because some of the used femaleheaders had very severe problems with soldertin adhesion. Later it was discovered, that this might have been related to cheap quality as the contact pins apparently where made form a different metal that had oxidized. Application of sandpaper to remove the oxidized layers and soldering flux were futile. All female headers have been replaced with another batch, which had perfect soldertin adhesion.

![](../images/final_project2/soldering/jumpers.jpg)

As these measures restored continuity, the pcb was not remade. After this 328p controller was soldered as this posed the second largest point of concern.

 ![](../images/final_project2/soldering/top.jpg)

After completing this, the other components have been soldered to the pcb.

 ![](../images/final_project2/soldering/board.jpg)

 While soldering the usb connector, an issue with the layout of the traces became apparent: the layout of the connections of the usb connector footprint in KiCAD did not match the tracelayout of the real connector. A solderbridge connecting these pins solved the issue.
  ![](../images/final_project2/soldering/usb.jpg)

## Downloads

[KiCAD_Project](docs\files\final_project2\KiCAD_Project.rar)
