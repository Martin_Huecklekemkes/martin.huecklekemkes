# 04. Electronics Production

## Task

- convert the provided plan into millable data
- mill the pcb according to plan
- place and solder the components
- program the ic using an adruino

## Preface

A plan with the layout, necessary components and pinout was provided to the students as basis.
It was the students task to use these plans to create the circuit as specified.
The students did not do any electronics design in this assignment.

## Plan conversion

The plan was provided in KiCAD format. First the layers of the plan had to be exported from KiCAD as an svg.

![](../images/week04/kicad.PNG)
<sup>*KiCAD export*<sup>

 After that the .svg files had to be converted to .png in inkscape. Last the image had to separated into individual work steps. GIMP is used for this task. The traces, the holes and the cutout were exported as individual pictures.

The pictures then have been used to generate the code that controls the mill, using the online tool [fabmodules](fabmodules.org).

First the image type hast to be selected, second, the output type has to be set, third the worktype has to be selected.
![](../images/week04/select.PNG)

last the machine settings had to bee determined
![](../images/week04/settings.PNG)
<sup>**settings not for reference*</sup>

The tool then outputs a path for the mill that look like this and can be loaded to the machine
![](../images/week04/output.PNG)

## Milling

The mill was a Roland MX40??? and was fitted with custom made vacuum bed to hold the workpiece in place.

The steps for milling are:

- turn mill, pc and vaccumpump on
- place workpiece on vaccumbed and  align it
- insert milling head and tighten it
- home x and y axis by driving the tool to the start position
- turn on the spindle to not damage the flue while slowly stepping the z axis down to set the z origin
- drive z up again
- import the path files
- start the job

![](../images/week04/millsoftware.jpg)
<sup>millcontrolsoftware</sup>

It is to note that to cut the traces a 0.2 mm flute was used. After the first pass the flute had to be changed to 0.8 mm for the holes and the cutout. The z axis has to be homed again. After that the job can be started.

![](../images/week04/milling_done.jpg)
<sup>*milling the threads done*<sup>

## Soldering

Before soldering, the edges are sanded for safer handling and the pcb is cleaned with acetone, to remove dust from milling and oil residue from production and touching.

From KiCAD a "bill of materials" can be exported.

| ID  | Descriptor      | Casing       | Quantity | Description |
| --- | --------------- | ------------ | -------- | ----------- |
| 1   | UNK1            | SOIC8        | 1        | ATTINY45SI  |
| 2   | R1              | R1206FAB     | 1        | 10K         |
| 3   | R2              | R1206FAB     | 1        | 499         |
| 4   | LED1            | LED1206FAB   | 1        | YELLOW      |
| 5   | C3              | 1206         | 1        | 100nF       |
| 6   | C1              | 1206         | 1        | 1uF         |
| 7   | C2              | 1206         | 1        | 10uF        |
| 8   | POWER_1,POWER_2 | 02P          | 2        | M02         |
| 9   | M5              | 05P          | 1        | M05         |
| 10  | RST1            | 1X01_LONGPAD | 1        | M01         |

For soldering 0,75 mm Sn96,5 Ag3,0 Cu0,5 solderingwire was used.

![](../images/week04/station.jpg)

For placement of the components, the circuit view from KiCAD was used.

![](../images/week04/kicad_threadsonly.PNG)

Due to problems in the milling process, the pcb had to be milled with much smaller threads than planed. The threads were even smaller than the contacts of the ic itself.

![](../images/week04/threads.jpg)
<sup>The traces are barely half the width of the ic contacts</sup>

Soldering all contacts of the ic to the pcb and cleaning afterwards with desolderingwick striped the thread of the pcb at multiple places. This has been fixed with copper jumperwires that have been soldered to the contact of the component and the intact part of the thread.

![](../images/week04/wires.jpg)
<sup>copper jumperwires soldered to the ic and the threads</sup>

Also, as lead free solder has been used, the solderingpoints are dull and not shiny.

Further the pcb has to be cleaned with isopropanol or other alcohol and a soft brush, to remove the flux residues.

The circuit has been inspected visually and tested with a multimeter and no shortcircuits have been found and the components that should be, are connected.

## Programing

The instructions for programming the ic are as follows:

1. patch the Arduino IDE with the following: http://highlowtech.org/?p=1695
   (Necessary to add support for the ATtiny45 to the arduino IDE; good explanation there, so it is omitted here)
2. take an Arduino Uno
3. make sure the Arduino has nothing connected to it
4. connect the Arduino to the pc with the USB cable
5. in Arduino IDE select the right port and the Arduino Uno board in tools
6. in File->Examples find and open the ArduinoISP sketch
7. upload the sketch to the Arduino
8. disconnect the Arduino from the pc
9. connect the hello board with the Arduino (check the connection schema image) <sup>1</sup>
10. triple check the connections are correct
11. connect the Arduino to the pc with the USB cable
12. select the right Board/Processor/Frequency -> attiny25/45/85, attiny45, internal 8mhz
13. under tools select the Arduino as ISP as the programmer
    (It is important here to select the right option "Arduino as ISP" and _not_ ArduinoISP, otherwise it will not work.) <sup>2</sup>
14. double check all the parameters
15. click to Tools-> Burn Bootloader
16. write your own program
17. to program the board do Sketch->Upload using programmer

<sup>1: Connections</sup>![](../images/week04/arduino_connection.png)

<sup>Hello Board connected.</sup>
![](../images/week04/IMG_20211029_120713.jpg)

<sup>2: Arduino as ISP</sup> ![](../images/week04/Arduino_as_ISP.PNG)

<sup>3: Code to load on the ATtiny45</sup>

```C++
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(2, OUTPUT);
}


// the loop function runs over and over again forever
void loop() {
  digitalWrite(2, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(2, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```

Can also be downloaded [here](../files/week04/blink_hello_board.ino)

After this, the Hello Board only needs the powerconnetion from the arduino, as the blink sketch was uploaded to the ic.

![](../images/week04/IMG_20211029_120735.jpg)
