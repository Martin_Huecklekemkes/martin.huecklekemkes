# 14. Final Project: Assembly

## Task

- manufacture all parts
- assemble the mill

## Preface

As so many things did not work out in this stage, this should rather be understood as an ideal assembly instruction. The problems probably will be mentioned shortly, but will be discussed more thoroughly in the retrospect chapter.

## Manufacture the parts

To create the cutting job was created using a testfeature called nesting. This allows to select the parts for milling and automatically lays them out in the specified stockvolume. Then only the milling operations had to be configured. In this case only the drilling operation for the 4 and 8mm holes and 2D-contour operations where used.

See nesting tutorial [here](https://www.youtube.com/watch?v=TIBMX-oVasU).

![](../images/final_project4/mill1.jpg)
![](../images/final_project4/mill2.jpg)
![](../images/final_project4/mill3.jpg)

The tabs have been sanded of on the beltsander.

Contrary to the design process, the assembly begins with the base continues with the gantry. After this both are joined as the y-axis bearings are slid onto the shafts that are attached to the base. Then the z-axis-carriage is assembled and is finally attached to the x-axis-carriage riding on the gantry.

### The base

The base consist of the woodenplates milled on the cnc machine.

Start by sliding in the vertical pieces into the recesses. They might have a very tight fit, but this is intended. If it does not fit even by force sand the edges of the vertical pieces and the inner edges of the pockets. Use a hammer to force them in. Clamp everything together and measure and mark the position of the screws in regular intervals. ~4cm were used here. Then predrill the marked holes with a appropriate drill matching the screw diameter and depth. The predrilling is necessary to prevent splintering in the vertical pieces, as the screws go into the end-grain. Also screw the front and rear to the left and right pieces.

The screws used here were 3,5x15mm woodscrews.

![](../images/final_project4/assembly2.jpg)
![](../images/final_project4/assembly3.jpg)

## The gantry

The procedure here is the same as with the base. Fit the vertical pieces into the recesses, clamp everything together, measure, mark and drill the holes, then screw everything together.

![](../images/final_project4/assembly1.jpg)

### Y-Axis

The holders of the y-axis are attached to the base. The holes for the shaft holders and the flangebearing use countersunk screws in M6 and M5 to give more clearance for the bearingblocks. One leg of the gantry can be preassembled with the bearingblocks and screwblock. The other side can only be done when placed over the the base, as the bearings would protrude to far out. To assemble the other leg align the spacerboards on the legs with the holes. Then put screws through some holes to keep the alignment. Screw the screwblock to the leg with M6 nuts and bolts. Then the bearings can be attached and screwed to it. After this slide the shafts trough the holders and the bearings. At last insert the leadscrew through through the flangebearing and screw it through the leadscrewnut.

![](../images/final_project4/assembly4.jpg)
![](../images/final_project4/assembly5.jpg)

### Z-Axis

Pressing one of the bearing into their respective blocks failed and destroyed the woodenblock. As the screwblocks still had to be made from plastics, it was decided to redo all the blocks for the z-axis from plastics.

![](../images/final_project4/remill1.jpg)
![](../images/final_project4/remill2.jpg)

After this the bearings where pressed into their casingblocks, which worked this time.

The leadscrewnut was also pressed into one of the bearingblocks, the 11x5mm bearings where pressed into the bottom piece.

The order of assembly of the remaining part is important, as some screws are not accessible when (partially)assembled.

The screws through the bearingblocks to connect to the frontplate have to be countersunk to save space.

The bottom piece is screwed to the backplate. Beware that countersunk screws have to be used here to be flush with the surface, as the backside will be screwed to another plate. The bearing casing blocks are screwed to the frontplate. The 8mm shafts were then pressed into the bottom piece and the bearingblocks attached to the frontplate where rolled on the shafts. Then the top piece is pressed onto the shafts and also screwed to the backplate.
![](../images/final_project3/zaxis.PNG)

### X-Axis

![](../images/final_project3/xaxis.PNG)

The assembly of the x-axis is similar to the y-axis. The x-carriage plate has to be screwed to the bearings and the screwblock to the plate. This is important as these screws are not accessible after assembly.

Screw the shaftholder to the gantry.

![](../images/final_project4/x-carriage.PNG)

### Joining X and Z carriages

After assembling the x-carriage and the z-carriage, they can be screwed together. The whole assembly can now be slid onto the shafts of the x-axis to finalize it.

Attach the steppermotors and insert the screws where not already done.

Lubricate the motionsystem.
