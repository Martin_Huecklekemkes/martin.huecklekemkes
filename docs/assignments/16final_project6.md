# 16. Final Project: Retrospect

## Omitted features

The use of endstopswitches definitely would have been a good addition, to prevent probable crashes of the carriages with the frame. However due to time concerns they have been omitted.

## The PCB Plans

The complexity of the plan creation has been underestimated. It took longer than anticipated and several plan iterations where necessary to remove all problems and still some issues creped in like the issue with the usb connector footprint not matching the real footprint. However, making the pcb doublesided made routing of some traces easier and keeped the pcb compact. Otherwise, using only a single sided board, routing the traces would have been more complicated, with longer traces and many 0 ohm resistors for bridging traces. Another option would have been to put the drivers on a separate board and to be connected via cables.

## The PCB milling

While preparing the milling of the pcb, some further and unexpected tasks emerged. For example to mill a piece of mdf flat to have a reference surface for the pcb to flip. Further the alignment holes had to be exported to a separate file as this was overlooked when preparing in the firstplace. In retrospect, milling the pcb double sided added a substantial amount of extra work, in form of the jumperwires that bridge the broken traces on the bottom side. In hindsight it probably would have been a better use of time to redo the pcb and try to adjust the settings so that the traces are not disrupted and do some other task while the pcb is on the mill.

## Construction plans

After some brainstorming and getting some inspiration, the construction was mostly without complications. Only Fusion360 sometimes behaved confusing and counter intuitive which also consumed a lot of time. The one big mistake that has been made in this step where the holes for the screws and the screws it self. The initial idea was to use countersunk screws to join the moving parts, so that the screws are flush with the surface to keep the assembly compact. Thus all the moving parts all had a spacing between 5mm and 10mm.

![](../images/final_project6/countersunkscrew.jpg)

However it was not checked which screws where available. Only after the assembly had been started it became apparent that just ordinary M6 hexhead screws were available, which where to tal for the needed purposes.

![](../images/final_project6/hexhead.jpg)

The sourcing of the needed screws was quiet difficult, as multiple hardware stores did not have the required sizes. In the end the screws had to be ordered online. Further the recesses for the screwheads have not been considered for milling, which would have been a cleaner and more consistent solution than countersinking the holes manually. Doing this cause further problems in later assembly, discussed in the next section. If the screw issue would have emerged earlier, the spacing could have been adjusted to fit regular screws.

## Milling the CNC parts

While preparing the pieces for milling, it was considered, that it would be a good idea, to added recesses e.g. to the base or the gantry, where the pieces that are perpendicular would slide in, to make it easier to fit all the pieces precisely and square together. After theses have been added, there where some other adjustments necessary which changed the position of the recesses. As this was added last minute, one pocket was overlooked and milled in the wrong position. In this case it was easy to correct the issue and widen the pocket manually.

Another issue occurred while pressfitting the 8mm bearings into the woodenblocks. One of the woodenblocks broke. As the screw blocks where planed to be milled from plastic from the beginning and still had to be done, it was decided to redo the bearing blocks and the attachment blocks for the z-axis also from plastics. When milling an accident occurred lifting the stock up from the bed, however it was not immediately obvious that the stock shifted. This only became apparent much later in the assembly, when parts did not fit together as intended. The bearingblocks and z-attachmentblocks where somehow usable with the offset, the screwblocks however, were unusable. They have been redone manually cutting squares from the stock on the bandsaw and refined to size on the beltsander. The holes have been drilled manually.

## Assembly

As hinted in the previous paragraph, countersinking the holes with a handdrill was a bad idea, because the tools used for this may have been a bit dull and caused the countersinking to shift of center. When assembling everything together, especially the motion parts, these off-center countersinkings introduced stress and small deformation, when the countersunk screws were tightened and pulled the piece off-center.

In retrospect it was a terrible idea to do manufacture some parts manually, because all these tiny inaccuracies introduced by manual manufacturing added up. As a consequence the motion of the leadscrews is very tight and stiff, especially near the limits. This can be mitigated partly by using some PTFE lubricant. Despite this the steppermotors frequently skip steps near the limits. These problems with the tight screws probably are only fixable by redoing all the parts interfacing with them to archive better tolerances.

## Operation

## Lessons learned

- no matter how good you check, there is always a fault or something overlooked that makes it into your first manufactured iteration (usb connector footprint, connector label, recess in baseplate)
- adding complexity can make things easier in one step, but more difficult in other steps (double sided pcb)
- adding complexity can introduce unforeseen new points of failure into the build (pcb bottomside)
- when planing around some given constraints/dimensions measure precise. Twice!
- redoing things correctly can save time and nerves compared fixing something and probably yields a better, tidier and less error prone result (pcb backside, screw-/bearingblocks)
- never assume something is available/in store, check what you can use before/while planing so you can adapt to missing parts (screws)
- don't be lazy, add ass much functional details (e.g. holes, countersinkings, recesses) to your construction model as possible, it will save you time measuring and marking later and makes assembly easier (countersunkscrews, woodscrew holes, recesses)
- don't tighten every nut and screw to be deadtight, one side of the motion assembly can use some play to compensate deviations
- avoid adding features last minute, they will most likely introduce some faults you overlook(see first point)
  - if absolutely necessary really take the time to check for faults; go from the modified part over all adjacent parts and ask yourself "what effect could this modification have?
- check the manufactured pieces for dimensional accuracy, to catch faults early (screwblocks, z-axis-attacher)
- always at least double the time you estimated. There will always be a problem that steals time. And if you don't follow the steps above, there will be lots of problems.
- you can and should try to think of everything that could be necessary for the project, but you will overlook something you later have to patch in (recesses in baseplate)

And the most important lesson: Unless you are a skilled craftsmen and have precise tools at your disposal, **avoid manual manufacturing as much as possible.**
Manual manufacture only introduces deviations. Let the cnc mill do as much work as possible and don't refrain from redoing it. It will save your sanity down the road.

If you really have to do some manual work, build some kind of jig, to get the step done precisely and repeatedly.

## Closing words

I learned a lot in this project, although it not went like i imagined. I would strongly recommend not to blindly copy this machine, as there are problems that have not been fixed. Rather you could use it as an inspiration and take the lessons learned into account to avoid the mistakes and troubles i went through. I am looking forward to build another, lager, cnc mill, for making furniture. I am confident that with my acquired knowledge this will be a better result than this mill.
