# 15. Final Project: Commissioning

## Task

- start mill
- move mill via gcode commands
- start a milling job

- flashing grbl to the arduino
- inserting drivers
- hooking up steppers
- configuring grbl
- UGS (universal gcode sender)
- measuring stepps
- starting job

## Controller setup

The bootloader for the 328p was burned using an arduino as isp. This was successful. After this it was tried to upload the grbl-firmware via the usb, which did not work. there was some issue somewhere in the usb system. As time was short it was decided to just skip the usb part and interface via ftdi with the 328p. This worked. It was possible to connect to the grbl via console and change settings.

## The drivers

Next one driver was inserted and a motor connected, to test if the motor can be controlled with the controller, which was unsuccessful. There also was a problem with the 12V connection. Both problems could not be solved.

## The accident

For unknown reasons the capacitors for the stepperdrivers exploded, rendering the controller unusable. It would have been possible, to replace the capacitors, but a further in depth inspection for other damaged parts would have been necessary.

![](../images/final_project5/damagedboard.jpg)

Thus it was decided to just use an arduino with an cnc shield.
![](../images/final_project5/cncshield.jpg)

## Starting a job

For testing purposes, a simple hemisphere was constructed in Fusion. It the 3D-adaptive clearing operation was used.

For controlling the cnc the software [UGS platform](https://winder.github.io/ugs_website/guide/platform/) has been used.

![](../images/final_project5/ugs.PNG)

With this software it is possible to control the movement of the cnc mill with the computer keyboard. Further it is possible to import gcode files to start a milling job. Otherwise the procedure is similar to the Roland mill used for pcb milling.

![](../images/final_project5/mill1.jpg)
![](../images/final_project5/mill2.jpg)
![](../images/final_project5/mill3.jpg)

## Downloads

A video of the mill in action can be downloaded [here](../images/final_project5/milling_video.mp4).

[Hemissphere Model](../files/final_project5/testrun4_hemisphere.nc)

To export the modell from Fusion as gcode and without the homing that is inserted by the default gcode postprocessor a  custom postpocessor had to be used.

It can be downloaded [here](https://github.com/fab-machines/TseNC/blob/main/grbl_tsenc_v1.cps).
